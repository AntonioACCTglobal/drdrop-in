import "./App.css";
import { useEffect } from "react";

function App() {
  useEffect(() => {
    let digitalriverpayments = new window.DigitalRiver(
      "",
      {
        locale: "en_US",
      }
    );

    let configuration = {
      sessionId: "20a7ac60-55af-4797-982d-5b9b5f9afd91",
      options: {
        flow: "checkout",
        showSavePaymentAgreement: false,
        button: {
          type: "payNow",
        },
        showComplianceSection: true,
        showTermsOfSaleDisclosure: false,
      },
      billingAddress: {
        firstName: "John",
        lastName: "Doe",
        email: "test@test.com",
        phoneNumber: "952-253-1234",
        address: {
          line1: "10380 Bren Road W",
          city: "Minnetonka",
          state: "MN",
          postalCode: "55343",
          country: "US",
        },
      },
      paymentMethodConfiguration: {
        enabledPaymentMethods: ["creditCard"],
      },
    };

    let dropin = digitalriverpayments.createDropin(configuration);
    dropin.mount("drop-in-container");
  }, []);

  return <div className="App" id="drop-in-container"></div>;
}

export default App;
